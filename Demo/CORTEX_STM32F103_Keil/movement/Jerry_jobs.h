#ifndef __JERRY_JOBS_H
#define __JERRY_JOBS_H

#include "stm32f10x_type.h"
#include "stdint.h"

#include "Jerry_movement.h"
#include "adc_handler.h"

//GYRO define
#define firstAngle											0

/*-----------------------------------------------------------*/
//ADC define

//Standard things
#define ADC_Standard_Center							0
#define ADC_Standard_Left								1
#define ADC_Standard_Right							2
#define ADC_Standard_fLeft							3
#define ADC_Standard_fRight							4
#define ADC_Standard_HasBothWall				5

//IS_Straight
#define HAVE_Front_Straight_Min					1000
#define HAVE_Front_Straight_Max					2000
#define HAVE_Side_Straight_Min					1000
#define HAVE_Side_Straight_Max					2000

//IS_Turn_Left
#define HAVE_SideLeft_Turn_Min					1000
#define HAVE_SideLeft_Turn_Max					2000

//IS_Turn_Right
#define HAVE_SideRight_Turn_Min					1000
#define HAVE_SideRight_Turn_Max					2000

//IS_Way_End
#define HAVE_Front_End_Min							1000
#define HAVE_Front_End_Max							2000

/*-----------------------------------------------------------*/

//PID define
#define P																5
#define I																0
#define D																6
#define leftBaseSpeed				(uint32_t)	1999
#define rightBaseSpeed  		(uint32_t)	1999

// RID param
#define	ANGLE_90  											25
#define	ANGLE_180  											90
#define	ANGLE_P90  											-5

/*-----------------------------------------------------------*/
//GYRO functions
/*
check if mourse go circle
*/
bool Jerry_Jobs_Gyro(float rotateAngle);

/*-----------------------------------------------------------*/
//ADC functions
/*
get the right thing from calibrate value
*/
void Jerry_Jobs_ADCInit(ADC_Value* calibrate);
/*
get the way status from adcValue
*/
int Jerry_Jobs_ADC(ADC_Value* adcValue);

/*
check adc in (Min, Max)
*/
static bool Jerry_Jobs_ADCCheck(ADC_Value adcValue, int Min, int Max);

/*-----------------------------------------------------------*/
//PID functions
/*
calculate speed for motor
*/
int32_t Jerry_Jobs_PID(ADC_Value* adcValue);

/*-----------------------------------------------------------*/
//Moving functions
/*
return moving status to Tom
*/
int Jerry_Jobs_MovingHandler(int command, Jerry_Peripheral* peri);

/*-----------------------------------------------------------*/
//Rolling functions
/*
rolling in the deep
*/
void Rolling_In_The_Deep(float cur_Angle, float pre_Angle, int8_t angle, int8_t *roll_stt);

#endif /* __JERRY_JOBS_H */
