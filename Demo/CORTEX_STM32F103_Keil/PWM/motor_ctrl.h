#ifndef __MOTOR_CTRL_H
#define __MOTOR_CTRL_H

#include "stm32f10x_tim.h"
#include "stm32f10x_map.h"
#include <stdint.h>

void PWM_GPIO_SetBit(GPIO_TypeDef* GPIOx, u16 GPIO_Pin, u8 value);
void RCC_Init_PWM(void);
void GPIO_Init_PWM(void);
void PWM_Init_Motor(void);
void PWM_Speed_Motor(int16_t left, int16_t right);

#endif /* __MOTOR_CTRL_H */
