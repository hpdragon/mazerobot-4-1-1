/* Define to prevent recursive inclusion*/
#ifndef __MY_BLINKY_H
#define __MY_BLINKY_H

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

#define			LED_OS_Rcc						RCC_APB2Periph_GPIOC
#define			LED_Battery_Rcc				RCC_APB2Periph_GPIOA
#define			LED_Debug_Rcc					RCC_APB2Periph_GPIOB

#define			LED_OS_Port						GPIOC
#define			LED_Battery_Port			GPIOA
#define			LED_Debug_Port				GPIOB

#define			LED_OS_Pin						(1UL << 13)
#define			LED_Battery_Pin				(1UL << 8)
#define			LED_Debug_Pin1				(1UL << 12)
#define			LED_Debug_Pin2				(1UL << 13)
#define			LED_Debug_Pin3				(1UL << 14)
#define			LED_Debug_Pin4				(1UL << 15)

#define			LED_OS_Blink_Speed		1000 / portTICK_PERIOD_MS

//init led
void Led_Init(void);
//blink led PC13
void vLed_Task(void *pvParameters);

#endif /* __BLINKY_H */
