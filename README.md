# Maze solving project - Dicky

This is a maze solving project (known as well micromourse). It was kindly named as Dicky. Dicky was created airm to participate in The Maze Runner contest hosted by PITIT University. It won 3rd price thought. This project contain all of our code during this contest.

# Overview:

![Alt text](image/image_01.jpeg?raw=false)

Dicky was build base on STM32F103C8T8 MCU. It uses FreeRTOS as the operation system.

Dicky uses it's sensor to know the surrounding enviroment (wall, way, ...). And use motor to moving around. In additional, it has a MPU6050 to detemire rotation andgle.

## System harware architecture:

- IR leds to identify it is wall or way.
- MPU6050 is used to replace encoder for rotating operation.
- 2 motors for movement.
- 2 battery cell for power entire system.

## System software architecture:

`

    Demo/
    | CORTEX_STM32F103_Keil/
    | | ADC/
    | | Blinky/
    | | Encoder/
    | | Finder/
    | | Flash/
    | | Init/
    | | MPU6050/
    | | Management/
    | | PWM/
    | | ParTest/
    | | RTE/
    | | STM32F10xFWLib/
    | | movement/
    | | serial/
    | | FreeRTOSConfig.h
    | | LCD_Message.h
    | | stm32f10x_conf.h
    | | main.c
    | | spi_flash.c
    | | timertest.c

`

In which:

- `Managemet` contains task that manage all other task
- `Movement` contains task that handle movement (motor, ...)
- `Finder` contains task that helps Dicky find its way
- others peripheral config

# Change log:

## Version 5.0

- Peripheral now can take order from Jerry literally
- Tom & Jerry now available

# Bug list:


